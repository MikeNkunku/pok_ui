package com.nkunku.trainings.pokui.domain.impl;

import java.time.LocalDate;
import java.time.chrono.ChronoZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.nkunku.trainings.pokui.domain.BirthData;
import com.nkunku.trainings.pokui.domain.BloodTypes;
import com.nkunku.trainings.pokui.domain.Genders;
import com.nkunku.trainings.pokui.domain.MaritalStatuses;
import com.nkunku.trainings.pokui.domain.Person;

public final class PersonDefaultImpl extends PersistableEntity<Integer, PersonDefaultImpl> implements Person {

	private static final String UNSPECIFIED_BLOOD_TYPE = "<UNKNOWN_BLOOD_TYPE>";
	private static final String UNSPECIFIED_PARENT = "<UNKNOWN_PARENT>";

	private BirthData birthData;
	private BloodTypes bloodType;
	private Collection<Person> children;
	private String code;
	private ChronoZonedDateTime<LocalDate> creationDate;
	private String currentFirstName;
	private Genders currentGender;
	private String currentLastName;
	private ChronoZonedDateTime<LocalDate> deathDate;
	private ChronoZonedDateTime<LocalDate> lastModificationDate;
	private Person lifePartner;
	private MaritalStatuses maritalStatus;
	private Collection<String> nicknames;
	private Collection<Person> siblings;
	private ChronoZonedDateTime<LocalDate> softDeletionDate;

	/**
	 * See {@link Builder} to create an instance.
	 */
	private PersonDefaultImpl() {}

	@Override
	public ChronoZonedDateTime<LocalDate> getBirthDate() {
		return birthData.getDate();
	}

	@Override
	public String getBirthFirstName() {
		return birthData.getFirstName();
	}

	@Override
	public Genders getBirthGender() {
		return birthData.getGender();
	}

	@Override
	public String getBirthLastName() {
		return birthData.getLastName();
	}

	@Override
	public BloodTypes getBloodType() {
		return bloodType;
	}

	private void setBloodType(final BloodTypes bloodType) {
		this.bloodType = Objects.requireNonNull(bloodType, "The provided blood type cannot be null");
	}

	@Override
	public Collection<Person> getChildren() {
		return children;
	}

	@Override
	public void setChildren(final Collection<Person> children) {
		this.children = Objects.requireNonNull(children, "The provided children cannot be a null set");
	}

	@Override
	public String getCurrentFirstName() {
		return currentFirstName;
	}

	private void setCurrentFirstName(final String firstName) {
		currentFirstName = firstName;
	}

	@Override
	public Genders getCurrentGender() {
		return currentGender;
	}

	private void setCurrentGender(final Genders gender) {
		currentGender = gender;
	}

	@Override
	public String getCurrentLastName() {
		return currentLastName;
	}

	private void setCurrentLastName(final String lastName) {
		currentLastName = lastName;
	}

	@Override
	public ChronoZonedDateTime<LocalDate> getDeathDate() {
		return deathDate;
	}

	private void setDeathDate(final ChronoZonedDateTime<LocalDate> dateOfDeath) {
		deathDate = dateOfDeath;
	}

	@Override
	public Person getFather() {
		return birthData.getFather();
	}

	@Override
	public Person getLifePartner() {
		return lifePartner;
	}

	@Override
	public void setLifePartner(final Person lifePartner) {
		this.lifePartner = lifePartner;
	}

	@Override
	public MaritalStatuses getMaritalStatus() {
		return maritalStatus;
	}

	private void setMaritalStatus(final MaritalStatuses maritalStatus) {
		this.maritalStatus = Objects.requireNonNull(maritalStatus, "The provided marital status cannot be null");
	}

	@Override
	public Person getMother() {
		return birthData.getMother();
	}

	@Override
	public Collection<String> getNicknames() {
		return nicknames;
	}

	private void setNicknames(final List<String> nicknames) {
		this.nicknames = Objects.requireNonNull(nicknames, "The provided nicknames cannot be a null set");
	}

	@Override
	public Collection<Person> getSiblings() {
		return siblings;
	}

	@Override
	public void setSiblings(final Collection<Person> siblings) {
		this.siblings = Objects.requireNonNull(siblings, "The provided siblings cannot be a null set");
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(final String code) {
		this.code = code;
	}

	@Override
	public ChronoZonedDateTime<LocalDate> getCreationDate() {
		return creationDate;
	}

	/**
	 * @implSpec It mustn't manually be set. It's the responsibility of the database.
	 */
	@SuppressWarnings("unused")
	private void setCreationDate(final ChronoZonedDateTime<LocalDate> creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public ChronoZonedDateTime<LocalDate> getLastModificationDate() {
		return lastModificationDate;
	}

	/**
	 * @implSpec It mustn't manually be set. It's the responsibility of the database.
	 */
	@SuppressWarnings("unused")
	private void setLastModificationDate(final ChronoZonedDateTime<LocalDate> lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}

	@Override
	public ChronoZonedDateTime<LocalDate> getSoftDeletionDate() {
		return softDeletionDate;
	}

	/**
	 * @implSpec It mustn't manually be set. It's the responsibility of the database.
	 */
	@SuppressWarnings("unused")
	private void setSoftDeletionDate(final ChronoZonedDateTime<LocalDate> softDeletionDate) {
		this.softDeletionDate = softDeletionDate;
	}

	@Override
	protected void enrichEntityDescription(final ToStringBuilder personStringBuilder) {
		personStringBuilder.append("code", code).append("currentFirstName", currentFirstName)
				.append("birthFirstName", getBirthFirstName())
				.append("nicknames", "[ " + String.join(", ", nicknames) + " ]")
				.append("currentLastName", currentLastName)
				.append("birthLastName", getBirthLastName())
				.append("currentGender", currentGender)
				.append("birthGender", getBirthGender())
				.append("birthDate", getBirthDate())
				.append("deathDate", deathDate)
				.append("mother", Optional.ofNullable(getMother()).map(Person::getLabel).orElse(UNSPECIFIED_PARENT))
				.append("father", Optional.ofNullable(getFather()).map(Person::getLabel).orElse(UNSPECIFIED_PARENT))
				.append("bloodType", Optional.ofNullable(bloodType).map(BloodTypes::label).orElse(UNSPECIFIED_BLOOD_TYPE))
				.append("maritalStatus", maritalStatus.label())
				.append("lifePartner", lifePartner.getLabel())
				.append("siblings", "[ "+ siblings.stream().map(Person::getLabel).collect(Collectors.joining(", ")) + " ]")
				.append("children", "[ " + children.stream().map(Person::getLabel).collect(Collectors.joining(", ")) + " ]")
				.append("creationDate", creationDate)
				.append("lastModificationDate", lastModificationDate)
				.append("softDeletionDate", softDeletionDate);
	}

	@Override
	protected boolean descendantFieldsEquals(final Object obj) {
		if (!(obj instanceof Person)) {
			return false;
		}

		final Person person = (Person) obj;
		return Objects.equals(code, person.getCode())
				&& Objects.equals(currentFirstName, person.getCurrentFirstName())
				&& Objects.equals(getBirthFirstName(), person.getBirthFirstName())
				&& Objects.equals(nicknames, person.getNicknames())
				&& Objects.equals(currentLastName, person.getCurrentLastName())
				&& Objects.equals(getBirthLastName(), person.getBirthLastName())
				&& Objects.equals(currentGender, person.getCurrentGender())
				&& Objects.equals(getBirthGender(), person.getBirthGender())
				&& Objects.equals(getBirthDate(), person.getBirthDate())
				&& Objects.equals(deathDate, person.getDeathDate())
				&& Objects.equals(getMother(), person.getMother())
				&& Objects.equals(getFather(), person.getFather())
				&& Objects.equals(bloodType, person.getBloodType())
				&& Objects.equals(maritalStatus, person.getMaritalStatus())
				&& Objects.equals(lifePartner, person.getLifePartner())
				&& Objects.equals(siblings, person.getSiblings())
				&& Objects.equals(children, person.getChildren());
	}

	@Override
	protected Collection<Object> getHashCodeFields() {
		final Collection<Object> fieldsToHashCode = new ArrayList<>();

		fieldsToHashCode.add(code);
		fieldsToHashCode.add(currentFirstName);
		fieldsToHashCode.add(getBirthFirstName());
		fieldsToHashCode.add(nicknames);
		fieldsToHashCode.add(currentLastName);
		fieldsToHashCode.add(getBirthLastName());
		fieldsToHashCode.add(currentGender);
		fieldsToHashCode.add(getBirthGender());
		fieldsToHashCode.add(getBirthDate());
		fieldsToHashCode.add(deathDate);
		fieldsToHashCode.add(getMother());
		fieldsToHashCode.add(getFather());
		fieldsToHashCode.add(bloodType);
		fieldsToHashCode.add(maritalStatus);
		fieldsToHashCode.add(lifePartner);
		fieldsToHashCode.add(siblings);
		fieldsToHashCode.add(children);

		return fieldsToHashCode;
	}

	@Override
	protected PersonDefaultImpl self() {
		return this;
	}

	public static class Builder {
		private PersonDefaultImpl instance;
		private BirthDataDefaultImpl.Builder birthDataBuilder;

		public Builder() {
			resetInstance();
		}

		public Person build() {
			instance.birthData = birthDataBuilder.build();
			final Person person = instance;
			resetInstance();
			return person;
		}

		public Builder withBirthDate(final ChronoZonedDateTime<LocalDate> birthDate) {
			birthDataBuilder.withDate(birthDate);
			return this;
		}

		public Builder withBirthFirstName(final String firstName) {
			birthDataBuilder.withFirstName(firstName);
			return this;
		}

		public Builder withBirthGender(final Genders gender) {
			birthDataBuilder.withGender(gender);
			return this;
		}

		public Builder withBirthLastName(final String lastName) {
			birthDataBuilder.withLastName(lastName);
			return this;
		}

		public Builder withBirthMiddleNames(final String... middleNames) {
			birthDataBuilder.withMiddleNames(middleNames);
			return this;
		}

		public Builder withBloodType(final BloodTypes bloodType) {
			instance.bloodType = bloodType;
			return this;
		}

		public Builder withChildren(final Person... children) {
			return withChildren(Arrays.asList(children));
		}

		public Builder withChildren(final Collection<? extends Person> children) {
			instance.children = new ArrayList<>(children);
			return this;
		}

		public Builder withCode(final String code) {
			instance.code = code;
			return this;
		}

		public Builder withDeathDate(final ChronoZonedDateTime<LocalDate> deathDate) {
			instance.deathDate = deathDate;
			return this;
		}

		public Builder withFather(final Person father) {
			birthDataBuilder.withFather(father);
			return this;
		}

		public Builder withCurrentFirstName(final String firstName) {
			instance.currentFirstName = firstName;
			return this;
		}

		public Builder withCurrentGender(final Genders gender) {
			instance.currentGender = gender;
			return this;
		}

		public Builder withCurrentLastName(final String lastName) {
			instance.currentLastName = lastName;
			return this;
		}

		public Builder withLifePartner(final Person lifePartner) {
			instance.lifePartner = lifePartner;
			return this;
		}

		public Builder withMaritalStatus(final MaritalStatuses maritalStatus) {
			instance.maritalStatus = maritalStatus;
			return this;
		}

		public Builder withMother(final Person mother) {
			birthDataBuilder.withMother(mother);
			return this;
		}

		public Builder withNicknames(final String... nicknames) {
			return withNicknames(Arrays.asList(nicknames));
		}

		public Builder withNicknames(final Collection<String> nicknames) {
			instance.nicknames = new ArrayList<>(nicknames);
			return this;
		}

		public Builder withSiblings(final Person... siblings) {
			return withSiblings(Arrays.asList(siblings));
		}

		public Builder withSiblings(final Collection<? extends Person> siblings) {
			instance.siblings = new ArrayList<>(siblings);
			return this;
		}

		private void resetInstance() {
			instance = new PersonDefaultImpl();
			birthDataBuilder = new BirthDataDefaultImpl.Builder();
		}
	}
}
