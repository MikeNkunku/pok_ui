package com.nkunku.trainings.pokui.domain;

/**
 * Interface which must be implemented by entities which have a code and the latter can be updated.
 */
public interface MutableCodifiable extends Codifiable {

	/**
	 * @param code The code to set.
	 */
	void setCode(String code);
}
