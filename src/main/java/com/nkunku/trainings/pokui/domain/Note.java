package com.nkunku.trainings.pokui.domain;

public interface Note extends Identifiable<Integer>, MutableCodifiable, Auditable, SoftDeletable {

	String getContent();
	String getTitle();
}
