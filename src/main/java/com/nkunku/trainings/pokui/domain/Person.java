package com.nkunku.trainings.pokui.domain;

import java.time.LocalDate;
import java.time.Period;
import java.time.chrono.ChronoZonedDateTime;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.text.WordUtils;

public interface Person extends Auditable, Identifiable<Integer>, MutableCodifiable, SoftDeletable {

	default Integer getAge() {
		return Integer.valueOf(Period.between(getBirthDate().toLocalDate(), LocalDate.now()).getYears());
	}

	ChronoZonedDateTime<LocalDate> getBirthDate();

	String getBirthFirstName();

	String getBirthLastName();

	Genders getBirthGender();

	BloodTypes getBloodType();

	Collection<Person> getChildren();

	void setChildren(Collection<Person> children);

	String getCurrentFirstName();

	Genders getCurrentGender();

	String getCurrentLastName();

	ChronoZonedDateTime<LocalDate> getDeathDate();

	Person getFather();

	default String getLabel() {
		return getCurrentFirstName() + " " + getCurrentLastName();
	}

	Person getLifePartner();

	void setLifePartner(Person lifePartner);

	default LivingStates getLivingState() {
		return isAlive() ? LivingStates.ALIVE : LivingStates.DEAD;
	}

	MaritalStatuses getMaritalStatus();

	Person getMother();

	Collection<String> getNicknames();

	Collection<Person> getSiblings();

	void setSiblings(Collection<Person> siblings);

	default boolean isAlive() {
		return getDeathDate() == null;
	}

	enum LivingStates {
		ALIVE("alive"),
		DEAD("dead");

		private final String id;

		LivingStates(final String identifier) {
			id = identifier;
		}

		/**
		 * @throws IllegalArgumentException When no living state could be found for the provided identifier.
		 */
		public static LivingStates from(final String identifier) {
			return Arrays.stream(values()).filter(livingState -> livingState.id.equals(identifier)).findFirst()
					.orElseThrow(() -> new IllegalArgumentException("No living state found for the \"" + identifier + "\" identifier"));
		}

		public String label() {
			return WordUtils.capitalizeFully(name());
		}
	}
}
