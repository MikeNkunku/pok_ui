package com.nkunku.trainings.pokui.domain;

import java.util.Arrays;

public enum MaritalStatuses {

	CIVIL_PARTNER("civilPartner", "In a civil union"),
	DIVORCED("divorced", "Divorced"),
	MARRIED("married", "Married"),
	SINGLE("single", "Single"),
	WIDOWED("widowed", "Widowed");

	private final String id;
	private final String label;

	MaritalStatuses(final String identifier, final String labelValue) {
		id = identifier;
		label = labelValue;
	}

	/**
	 * @throws IllegalArgumentException When no corresponding value is found for the provided identifier.
	 */
	public static MaritalStatuses from(final String identifier) {
		return Arrays.stream(values()).filter(maritalStatus -> maritalStatus.id.equals(identifier)).findFirst()
				.orElseThrow(() -> new IllegalArgumentException("No value for found the \"" + identifier + "\" identifier"));
	}

	public String label() {
		return label;
	}
}
