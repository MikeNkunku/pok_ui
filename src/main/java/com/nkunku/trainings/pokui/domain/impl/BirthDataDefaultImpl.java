package com.nkunku.trainings.pokui.domain.impl;

import java.time.LocalDate;
import java.time.chrono.ChronoZonedDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.nkunku.trainings.pokui.domain.BirthData;
import com.nkunku.trainings.pokui.domain.Genders;
import com.nkunku.trainings.pokui.domain.Person;

public final class BirthDataDefaultImpl implements BirthData {

	private ChronoZonedDateTime<LocalDate> date;
	private Person father;
	private String firstName;
	private Genders gender;
	private String lastName;
	private Set<String> middleNames;
	private Person mother;

	/**
	 * See {@link Builder} to build an instance.
	 */
	private BirthDataDefaultImpl() {}

	@Override
	public ChronoZonedDateTime<LocalDate> getDate() {
		return date;
	}

	void setDate(final ChronoZonedDateTime<LocalDate> birthDate) {
		date = birthDate;
	}

	@Override
	public Person getFather() {
		return father;
	}

	void setFather(final Person dad) {
		father = dad;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	void setFirstName(final String firstname) {
		firstName = firstname;
	}

	@Override
	public Genders getGender() {
		return gender;
	}

	void setGender(final Genders birthGender) {
		gender = birthGender;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	void setLastName(final String lastname) {
		lastName = lastname;
	}

	@Override
	public Set<String> getMiddleNames() {
		return middleNames;
	}

	void setMiddleNames(final Set<String> middlenames) {
		middleNames = Objects.requireNonNull(middlenames, "The set of middle names cannot be null");
	}

	@Override
	public Person getMother() {
		return mother;
	}

	void setMother(final Person mom) {
		mother = mom;
	}

	@Override
	public boolean equals(final Object object) {
		if (!(object instanceof BirthData)) {
			return false;
		}

		final BirthData birthData = (BirthData) object;
		return Objects.equals(date, birthData.getDate()) && Objects.equals(gender, birthData.getGender())
				&& Objects.equals(firstName, birthData.getFirstName())
				&& Objects.equals(middleNames, birthData.getMiddleNames())
				&& Objects.equals(lastName, birthData.getLastName())
				&& Objects.equals(mother, birthData.getMother())
				&& Objects.equals(father, birthData.getFather());
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, gender, firstName, middleNames, lastName, mother, father);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
				.append("date", date)
				.append("gender", gender.label())
				.append("firstName", firstName)
				.append("middleNames", middleNames)
				.append("lastName", lastName)
				.append("mother", mother.getLabel())
				.append("father", father.getLabel())
				.toString();
	}

	public static class Builder {
		private BirthDataDefaultImpl instance;

		public Builder() {
			resetInstance();
		}

		public BirthData build() {
			final BirthData birthData = instance;
			resetInstance();
			return birthData;
		}

		public Builder withDate(final ChronoZonedDateTime<LocalDate> date) {
			instance.setDate(date);
			return this;
		}

		public Builder withFather(final Person father) {
			instance.setFather(father);
			return this;
		}

		public Builder withFirstName(final String firstName) {
			instance.setFirstName(firstName);
			return this;
		}

		public Builder withGender(final Genders gender) {
			instance.setGender(gender);
			return this;
		}

		public Builder withLastName(final String lastName) {
			instance.setLastName(lastName);
			return this;
		}

		public Builder withMiddleNames(final String... middleNames) {
			instance.setMiddleNames(new HashSet<>(Arrays.asList(middleNames)));
			return this;
		}

		public Builder withMother(final Person mother) {
			instance.setMother(mother);
			return this;
		}

		private void resetInstance() {
			instance = new BirthDataDefaultImpl();
		}
	}
}
