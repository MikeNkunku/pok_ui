package com.nkunku.trainings.pokui.domain;

import java.io.Serializable;

/**
 * Interface enabling to identify an entity.
 *
 * @param <ID> The concrete serializable type of identifier.
 */
@FunctionalInterface
public interface Identifiable<ID extends Serializable> {

	/**
	 * @return The entity identifier.
	 */
	ID getId();
}
