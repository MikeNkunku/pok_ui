package com.nkunku.trainings.pokui.domain;

import java.util.Arrays;

public enum Genders {

	FEMALE("F"),
	MALE("M");

	private final String id;

	Genders(final String identifier) {
		id = identifier;
	}

	/**
	 * @throws IllegalArgumentException When no gender could be found for the provided identifier.
	 */
	public static Genders from(final String identifier) {
		return Arrays.stream(values()).filter(gender -> gender.id.equals(identifier)).findFirst()
				.orElseThrow(() -> new IllegalArgumentException("No gender found for the \"" + identifier + "\" identifier"));
	}

	public String label() {
		return id + name().substring(1).toLowerCase();
	}
}
