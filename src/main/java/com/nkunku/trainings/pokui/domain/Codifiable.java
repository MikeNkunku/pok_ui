package com.nkunku.trainings.pokui.domain;

/**
 * Interface to be implemented by entities which have a code.
 */
@FunctionalInterface
public interface Codifiable {

	/**
	 * @return The code of the entity.
	 */
	String getCode();
}
