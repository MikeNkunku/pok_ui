package com.nkunku.trainings.pokui.domain;

import java.time.LocalDate;
import java.time.chrono.ChronoZonedDateTime;
import java.util.Set;

public interface BirthData {

	ChronoZonedDateTime<LocalDate> getDate();

	Person getFather();

	String getFirstName();

	Genders getGender();

	String getLastName();

	Set<String> getMiddleNames();

	Person getMother();
}
