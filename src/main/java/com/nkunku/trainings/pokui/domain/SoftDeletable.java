package com.nkunku.trainings.pokui.domain;

import java.time.LocalDate;
import java.time.chrono.ChronoZonedDateTime;

public interface SoftDeletable {

	ChronoZonedDateTime<LocalDate> getSoftDeletionDate();

	default boolean isSoftDeleted() {
		return getSoftDeletionDate() != null;
	}
}
