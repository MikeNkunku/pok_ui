package com.nkunku.trainings.pokui.domain.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.nkunku.trainings.pokui.domain.Identifiable;

/**
 * Class holding the common behaviour and methods for entities that can be persisted.
 *
 * @param <ID> The concrete type of the identifier.
 * @param <ENTITY> The concrete type of the entity.
 */
public abstract class PersistableEntity<ID extends Serializable, ENTITY extends PersistableEntity<ID, ENTITY>>
		implements Identifiable<ID> {

	/** The instance identifier. */
	private ID id;

	/**
	 * Default constructor.
	 */
	protected PersistableEntity() {}

	/**
	 * @implSpec No need to enrich with the {@code id} field.
	 */
	protected abstract void enrichEntityDescription(ToStringBuilder entityStringBuilder);

	/**
	 * @implSpec The ID field must not be taken into account (already handled by the parent class).
	 * @param obj The object to compare.
	 * @return Whether the provided object is equal to the current instance.
	 */
	protected abstract boolean descendantFieldsEquals(Object obj);

	/**
	 * @implSpec <ul>
	 *     <li>The ID field must not be included (already handled in the parent class).</li>
	 *     <li>The returned collection can neither be {@code null} nor an immutable collection.</li>
	 * </ul>
	 * @return The collection of field values to take into account in order to produce the {@code hashCode} of the entity.
	 */
	protected abstract Collection<Object> getHashCodeFields();

	/**
	 * @implSpec Cannot be {@code null}.
	 * @return The current entity instance.
	 */
	protected abstract ENTITY self();

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof PersistableEntity)) {
			return false;
		}

		return Objects.equals(id, ((Identifiable<?>) obj).getId()) && descendantFieldsEquals(obj);
	}

	@Override
	public int hashCode() {
		final Collection<Object> hashCodeFields = new ArrayList<>(1);
		hashCodeFields.add(id);

		final Collection<Object> additionalHashCodeFields = Objects.requireNonNull(getHashCodeFields(),
				"The provided collection of fields to use in order to produce the entity hash code cannot be null");
		hashCodeFields.addAll(additionalHashCodeFields);

		return Objects.hash(hashCodeFields.toArray());
	}

	@Override
	public String toString() {
		final ToStringBuilder entityStringBuilder = new ToStringBuilder(self(), ToStringStyle.JSON_STYLE).append("id", this.id);
		enrichEntityDescription(entityStringBuilder);

		return entityStringBuilder.toString();
	}

	@Override
	public ID getId() {
		return id;
	}

	/**
	 * @implNote It mustn't be manually set since it's the responsibility of the database.
	 * @param id The entity identifier.
	 */
	@SuppressWarnings("unused")
	private void setId(final ID id) {
		this.id = id;
	}
}
