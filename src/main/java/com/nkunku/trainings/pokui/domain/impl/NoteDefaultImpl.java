package com.nkunku.trainings.pokui.domain.impl;

import java.time.LocalDate;
import java.time.chrono.ChronoZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.nkunku.trainings.pokui.domain.Note;

/**
 * Unique domain entity of this project whose only attributes besides its ID are its title and content.
 */
public final class NoteDefaultImpl extends PersistableEntity<Integer, NoteDefaultImpl> implements Note {

	private String code;
	private String content;
	private String title;
	private ChronoZonedDateTime<LocalDate> creationDate;
	private ChronoZonedDateTime<LocalDate> lastModificationDate;
	private ChronoZonedDateTime<LocalDate> softDeletionDate;

	NoteDefaultImpl() {
		this("", "", "");
	}

	public NoteDefaultImpl(final String code, final String title, final String content) {
		super();
		this.code = Objects.requireNonNull(code, "The note code cannot be null");
		this.title = Objects.requireNonNull(title, "The note title cannot be null");
		this.content = Objects.requireNonNull(content, "The note content cannot be null");
	}

	@Override
	protected void enrichEntityDescription(final ToStringBuilder noteStringBuilder) {
		noteStringBuilder.append("code", code).append("title", title).append("content", content)
				.append("creationDate", creationDate).append("lastModificationDate", lastModificationDate)
				.append("softDeletionDate", softDeletionDate);
	}

	@Override
	protected boolean descendantFieldsEquals(final Object obj) {
		if (!(obj instanceof Note)) {
			return false;
		}

		final Note note = (Note) obj;
		return Objects.equals(code, note.getCode())
				&& Objects.equals(title, note.getTitle()) && Objects.equals(content, note.getContent());
	}

	@Override
	protected Collection<Object> getHashCodeFields() {
		final Collection<Object> fieldsToHashCode = new ArrayList<>();
		fieldsToHashCode.add(code);
		fieldsToHashCode.add(title);
		fieldsToHashCode.add(content);

		return fieldsToHashCode;
	}

	@Override
	public String getContent() {
		return content;
	}

	public void setContent(final String content) {
		this.content = content;
	}

	@Override
	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(final String code) {
		this.code = code;
	}

	@Override
	protected NoteDefaultImpl self() {
		return this;
	}

	@Override
	public ChronoZonedDateTime<LocalDate> getCreationDate() {
		return creationDate;
	}

	/**
	 * @implSpec It mustn't manually be set. It's the responsibility of the database.
	 */
	@SuppressWarnings("unused")
	private void setCreationDate(final ChronoZonedDateTime<LocalDate> creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public ChronoZonedDateTime<LocalDate> getLastModificationDate() {
		return lastModificationDate;
	}

	/**
	 * @implSpec It mustn't manually be set. It's the responsibility of the database.
	 */
	@SuppressWarnings("unused")
	private void setLastModificationDate(final ChronoZonedDateTime<LocalDate> lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}

	@Override
	public ChronoZonedDateTime<LocalDate> getSoftDeletionDate() {
		return softDeletionDate;
	}

	/**
	 * @implSpec It mustn't manually be set. It's the responsibility of the database.
	 */
	@SuppressWarnings("unused")
	private void setSoftDeletionDate(final ChronoZonedDateTime<LocalDate> softDeletionDate) {
		this.softDeletionDate = softDeletionDate;
	}
}
