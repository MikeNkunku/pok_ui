package com.nkunku.trainings.pokui.domain;

import java.time.LocalDate;
import java.time.chrono.ChronoZonedDateTime;

/**
 * Interface to be implemented by entities which hold creation and last modification dates.
 */
public interface Auditable {

	/**
	 * @return The date at which the entity was created.
	 */
	ChronoZonedDateTime<LocalDate> getCreationDate();

	/**
	 * @return The last date at which the entity was modified.
	 */
	ChronoZonedDateTime<LocalDate> getLastModificationDate();
}
