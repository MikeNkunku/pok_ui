package com.nkunku.trainings.pokui.domain;

import java.util.Arrays;

public enum BloodTypes {

	A_NEGATIVE("A-"),
	A_POSITIVE("A+"),
	AB_NEGATIVE("AB-"),
	AB_POSITIVE("AB+"),
	B_NEGATIVE("B-"),
	B_POSITIVE("B+"),
	O_NEGATIVE("O-"),
	O_POSITIVE("O+");

	private final String id;

	BloodTypes(final String identifier) {
		id = identifier;
	}

	/**
	 * @throws IllegalArgumentException When no blood type can be found for the provided identifier.
	 */
	public static BloodTypes from(final String identifier) {
		return Arrays.stream(values()).filter(bloodType -> bloodType.id.equals(identifier)).findFirst()
				.orElseThrow(() -> new IllegalArgumentException("No value found for the \"" + identifier + "\" identifier"));
	}

	public String label() {
		return id;
	}
}
