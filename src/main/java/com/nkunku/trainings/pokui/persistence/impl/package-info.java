/**
 * Package containing implementation classes to manage (e.g., load, search) persisted entities.
 */
package com.nkunku.trainings.pokui.persistence.impl;
