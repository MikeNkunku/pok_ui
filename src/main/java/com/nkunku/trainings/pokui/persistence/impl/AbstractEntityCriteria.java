package com.nkunku.trainings.pokui.persistence.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;

import com.nkunku.trainings.pokui.domain.Identifiable;
import com.nkunku.trainings.pokui.persistence.EntityCriteria;

/**
 * Basic class which is to be extended by any entity criteria.
 *
 * @param <SERIALIZABLE_ID> The concrete type of serializable identifier.
 * @param <ENTITY> The concrete type of entity to look for.
 * @param <CRITERIA> The concrete type of criteria.
 */
public abstract class AbstractEntityCriteria<SERIALIZABLE_ID extends Serializable, ENTITY extends Identifiable<SERIALIZABLE_ID>,
		CRITERIA extends AbstractEntityCriteria<SERIALIZABLE_ID, ENTITY, CRITERIA>> implements EntityCriteria<ENTITY> {

	/** The identifier to filter on. */
	private SERIALIZABLE_ID idToSearch;

	/**
	 * @return The current instance of criteria.
	 * @implSpec {@code null} cannot be returned.
	 */
	protected abstract CRITERIA self();

	/**
	 * Adds the elements of the first collection to the second one if the latter is not null
	 * and the former is neither {@code null} nor empty.
	 *
	 * @param typeCollection The collection whose elements must be added.
	 * @param clearDestinationFieldBeforehand Whether to clear the destination field before inserting the provided values.
	 * @param destinationField The destination collection.
	 * @param <TYPE> The concrete type of elements to add.
	 * @return The current criteria instance.
	 */
	protected <TYPE> CRITERIA withValuesForField(final Collection<? extends TYPE> typeCollection,
			final boolean clearDestinationFieldBeforehand, final Collection<TYPE> destinationField) {
		Objects.requireNonNull(destinationField, "The destination field cannot be null");
		if (clearDestinationFieldBeforehand) {
			destinationField.clear();
		}
		if (CollectionUtils.isNotEmpty(typeCollection)) {
			destinationField.addAll(typeCollection);
		}
		return self();
	}

	/**
	 * @return The identifier to filter on.
	 */
	public SERIALIZABLE_ID getId() {
		return idToSearch;
	}

	/**
	 * @param id The single identifier to filter on.
	 * @return The current instance of criteria.
	 */
	public CRITERIA withId(final SERIALIZABLE_ID id) {
		idToSearch = Objects.requireNonNull(id, "The provided identifier cannot be null");
		return self();
	}
}
