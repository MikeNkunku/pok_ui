package com.nkunku.trainings.pokui.persistence;

import java.io.Serializable;

import com.nkunku.trainings.pokui.domain.Identifiable;

/**
 * Interface which is to be implemented by any criteria which is used to retrieve entities from database.
 *
 * @param <ENTITY> The concrete entity type to look for.
 */
public interface EntityCriteria<ENTITY extends Identifiable<? extends Serializable>> {

	/**
	 * @return The class object of the entity to retrieve.
	 */
	Class<ENTITY> getEntityClass();
}
