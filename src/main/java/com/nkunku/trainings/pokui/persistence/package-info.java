/**
 * Package containing interfaces which are persistence-related (e.g., interfaces which enables to retrieve an entity).
 */
package com.nkunku.trainings.pokui.persistence;
