package com.nkunku.trainings.pokui.persistence.exceptions;

/**
 * Basic exception class for database-related exceptions.
 */
public class DatabaseException extends RuntimeException {

	/**
	 * Cannot be instantiated this way. Use {@link #DatabaseException(String, Throwable) the 2-args constructor} instead.
	 */
	private DatabaseException() {}

	/**
	 * 2-args constructor which helps understanding why the database error occurred in a succint manner.
	 *
	 * @param detailedMessage An expressive message which helps understanding why the exception occurred.
	 * @param exception The exception which occurred.
	 */
	public DatabaseException(final String detailedMessage, final Throwable exception) {
		super(detailedMessage, exception);
	}
}
