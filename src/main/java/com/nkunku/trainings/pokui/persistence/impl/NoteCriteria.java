package com.nkunku.trainings.pokui.persistence.impl;

import java.util.ArrayList;
import java.util.Collection;

import com.nkunku.trainings.pokui.domain.Note;

/**
 * Criteria dedicated to finding note instances.
 */
public class NoteCriteria extends AbstractEntityCriteria<Integer, Note, NoteCriteria> {

	/** The collection of codes to filter on. */
	private final Collection<String> codes;

	/** The collection of titles to filter on.  */
	private final Collection<String> titles;

	/**
	 * Default constructor.
	 */
	public NoteCriteria() {
		super();
		codes = new ArrayList<>();
		titles = new ArrayList<>();
	}

	@Override
	public Class<Note> getEntityClass() {
		return Note.class;
	}

	/**
	 * @return The codes to filter on.
	 */
	public Collection<String> getCodes() {
		return codes;
	}

	/**
	 * @return The titles to filter on.
	 */
	public Collection<String> getTitles() {
		return titles;
	}

	@Override
	protected NoteCriteria self() {
		return this;
	}

	/**
	 * @param codeCollection The collection of codes to filter on.
	 * @return The current criteria instance.
	 */
	public NoteCriteria withCodes(final Collection<String> codeCollection) {
		return withValuesForField(codeCollection, true, codes);
	}

	/**
	 * @param titleCollection The collection of titles to filter on.
	 * @return The current criteria instance.
	 */
	public NoteCriteria withTitles(final Collection<String> titleCollection) {
		return withValuesForField(titleCollection, true, titles);
	}
}
