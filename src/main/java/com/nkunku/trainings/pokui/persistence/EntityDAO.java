package com.nkunku.trainings.pokui.persistence;

import java.io.Serializable;
import java.util.Collection;

import com.nkunku.trainings.pokui.domain.Identifiable;

/**
 * Interface to be implemented by any data access object (DAO) in this project.
 * @param <ENTITY> The concrete type of entity to be persistence-managed.
 */
public interface EntityDAO<ENTITY extends Identifiable<? extends Serializable>> {

	/**
	 * Deletes the entity whose identifier is provided.
	 *
	 * @param identifier The identifier of the entity to delete.
	 * @throws IllegalArgumentException When the entity is not found.
	 * @throws IllegalStateException When the entity cannot be deleted because of a constraint violation.
	 * @throws com.nkunku.trainings.pokui.persistence.exceptions.DatabaseException When the entity could not be deleted
	 * because of a database error.
	 */
	void deleteEntity(final Serializable identifier);

	/**
	 * Retrieves corresponding entities according to the filters in the provided criteria.
	 *
	 * @param criteria The criteria instance which holds the various filters.
	 * @return A collection of the corresponding entities.
	 * @throws com.nkunku.trainings.pokui.persistence.exceptions.DatabaseException When the corresponding entities could not
	 * be retrieved because of a database error.
	 */
	Collection<ENTITY> getEntities(final EntityCriteria<ENTITY> criteria);

	/**
	 * Retrieves the entity whose identifier has been provided.
	 *
	 * @param identifier The identifier of the entity to retrieve.
	 * @return The corresponding entity.
	 * @throws IllegalArgumentException When there's no entity associated with the provided identifier.
	 * @throws com.nkunku.trainings.pokui.persistence.exceptions.DatabaseException When the entity could not be retrieved
	 * because of a database error.
	 */
	ENTITY getEntity(final Serializable identifier);

	/**
	 * Persists or updates the provided entity.
	 *
	 * @param entity The entity to persist/update.
	 * @throws IllegalStateException When the entity cannot be neither persisted nor updated because of constraint violations.
	 * @throws com.nkunku.trainings.pokui.persistence.exceptions.DatabaseException When the entity could not be persisted or
	 * updated because of a database error.
	 */
	void saveOrUpdate(final ENTITY entity);
}
