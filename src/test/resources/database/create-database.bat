:: Documentation to read: https://www.postgresql.org/docs/12/manage-ag-createdb.html

:: Check if the java_pok_ui DB server group already exists and create it if it doesn't

:: Check if the "test" server exists and create if it doesn't

:: Check if the java_pok_ui_test owner exists and create it if it doesn't

:: Check if java_pok_ui_test has the ownership to the "test" server and give it the ownership if not already the case

:: Check java_pok_ui_test privileges on the "test" server and upgrade them if necessary
