package com.nkunku.trainings.pokui.persistence.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import org.junit.jupiter.api.Test;

import com.nkunku.trainings.pokui.domain.Identifiable;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test support class for entity criteria objects.
 *
 * @param <SERIALIZABLE_ID> The concrete type of serializable identifier.
 * @param <ENTITY> The concrete type of the entity to look for.
 * @param <CRITERIA> The concrete type of criteria.
 */
abstract class EntityCriteriaTestSupport<SERIALIZABLE_ID extends Serializable, ENTITY extends Identifiable<SERIALIZABLE_ID>,
		CRITERIA extends AbstractEntityCriteria<SERIALIZABLE_ID, ENTITY, CRITERIA>> {

	/**
	 * @implSpec Cannot be {@code null}.
	 * @return A non-null serializable identifier.
	 */
	protected abstract SERIALIZABLE_ID arbitraryId();

	/**
	 * @implSpec Cannot return {@code null}.
	 * @return A new criteria instance.
	 */
	protected abstract CRITERIA newInstance();

	/**
	 * Verifies the initial values of the criteria.
	 *
	 * @implSpec The body mustn't be empty.
	 *
	 * @param criteria The new criteria instance.
	 */
	protected abstract void verifyInitialFields(CRITERIA criteria);

	@Test
	void defaultConstructor() {
		final CRITERIA criteria = newInstance();
		verifyInitialFields(criteria);
	}

	@Test
	void id() {
		final SERIALIZABLE_ID id = Objects.requireNonNull(arbitraryId(), "The arbitrary identifier cannot be null");
		final CRITERIA criteria = newInstance().withId(id);
		assertSame(id, criteria.getId(), "The identifier should be the same");
	}

	@Test
	void withValuesForField_notClearingDestinationFieldBeforehand() {
		final Collection<String> destinationField = new ArrayList<>(2);
		destinationField.add("val1");

		newInstance().withValuesForField(Collections.singleton("val2"), false, destinationField);

		assertEquals(2, destinationField.size(), "The list should contain 2 elements");
		assertTrue(destinationField.containsAll(Arrays.asList("val1", "val2")),
				"\"val1\" & \"val2\" should be present");
	}
}
