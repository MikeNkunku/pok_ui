package com.nkunku.trainings.pokui.persistence.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.junit.jupiter.api.Test;

import com.nkunku.trainings.pokui.domain.Note;

import static org.junit.jupiter.api.Assertions.*;

class NoteCriteriaTest extends EntityCriteriaTestSupport<Integer, Note, NoteCriteria> {

	@Override
	protected Integer arbitraryId() {
		return Integer.valueOf(1);
	}

	@Test
	void codes_notEmpty() {
		final String code = "code";

		final NoteCriteria noteCriteria = new NoteCriteria().withCodes(Collections.singleton(code));

		final Collection<String> codes = noteCriteria.getCodes();
		assertEquals(1, codes.size(), "There should only be one element");
		assertTrue(codes.contains(code), "The expected code should be present");
	}

	@Test
	void titles_notEmpty() {
		final String title = "title";

		final NoteCriteria noteCriteria = new NoteCriteria().withTitles(Collections.singleton(title));

		final Collection<String> titles = noteCriteria.getTitles();
		assertEquals(1, titles.size(), "There should only be one element");
		assertTrue(titles.contains(title), "The expected title should be present");
	}

	@Test
	void getEntityClass() {
		assertSame(Note.class, new NoteCriteria().getEntityClass(), "The Note class object is expected");
	}

	@Override
	protected NoteCriteria newInstance() {
		return new NoteCriteria();
	}

	@Override
	protected void verifyInitialFields(final NoteCriteria criteria) {
		final Collection<String> codes = criteria.getCodes();
		assertTrue(codes.isEmpty(), "The collection of codes should be empty");
		assertTrue(codes instanceof ArrayList, "The collection of codes should be of ArrayList type");

		final Collection<String> titles = criteria.getTitles();
		assertTrue(titles.isEmpty(), "The collection of titles should be empty");
		assertTrue(titles instanceof ArrayList, "The collection of titles should be of ArrayList type");
	}
}
