package com.nkunku.trainings.pokui.persistence.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DatabaseExceptionTest {

	@Test
	void constructorWithTwoArgs() {
		final String detailedMessage = "Exception detailed message";
		final Exception exception = new Exception("Root exception");

		final DatabaseException databaseException = new DatabaseException(detailedMessage, exception);

		assertSame(detailedMessage, databaseException.getMessage(), "The message should be the same");
		assertSame(exception, databaseException.getCause(), "The cause should be the same");
	}

	@Test
	void defaultConstructor_exception() {
		try {
			DatabaseException.class.newInstance();
			fail("An exception should have been thrown by then");
		} catch (final Exception exception) {
			assertTrue(exception instanceof IllegalAccessException, "The exception should be a IllegalAccessException");
		}
	}
}
