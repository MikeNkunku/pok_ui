package com.nkunku.trainings.pokui.domain.impl;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;

import com.nkunku.trainings.pokui.domain.BirthData;
import com.nkunku.trainings.pokui.domain.Genders;
import com.nkunku.trainings.pokui.domain.Person;

import static org.junit.jupiter.api.Assertions.*;

class BirthDataDefaultImplTest {

	@Test
	void __equals() {
		final BirthData izukuMidoriyaBirthData = buildIzukuMidoriyaBirthData();

		assertEquals(izukuMidoriyaBirthData, izukuMidoriyaBirthData);

		assertNotEquals(izukuMidoriyaBirthData, Integer.valueOf(1));
		assertNotEquals(izukuMidoriyaBirthData, buildOchacoUrarakaBirthData());
	}

	private static BirthData buildIzukuMidoriyaBirthData() {
		final ChronoZonedDateTime<LocalDate> birthDate = ZonedDateTime.of(2004, 7, 15, 8, 33, 58, 0, ZoneId.of("GMT+9"));

		return new BirthDataDefaultImpl.Builder().withFirstName("Izuku").withLastName("Midoriya").withGender(Genders.MALE)
				.withDate(birthDate)
				.withMiddleNames("Ichi", "Ni", "San")
				.withMother(buildInkoMidoriyaSimplifiedRepresentation())
				.withFather(buildHisashiMidoriyaSimplifiedRepresentation())
				.build();
	}

	private static Person buildHisashiMidoriyaSimplifiedRepresentation() {
		return new PersonDefaultImpl.Builder().withCurrentFirstName("Hisashi")
				.withCurrentLastName("Midoriya")
				.withCurrentGender(Genders.MALE)
				.build();
	}

	private static Person buildInkoMidoriyaSimplifiedRepresentation() {
		return new PersonDefaultImpl.Builder().withCurrentFirstName("Inko")
				.withCurrentLastName("Midoriya")
				.withCurrentGender(Genders.FEMALE)
				.build();
	}

	private static BirthData buildOchacoUrarakaBirthData() {
		return new BirthDataDefaultImpl.Builder().withFirstName("Ochaco").withLastName("Uraraka").withGender(Genders.FEMALE).build();
	}

	@Test
	void __hashCode() {
		final BirthData izukuMidoriyaBirthData = buildIzukuMidoriyaBirthData();
		final int expectedHashCode = getExpectedHashCode(izukuMidoriyaBirthData);
		assertEquals(expectedHashCode, izukuMidoriyaBirthData.hashCode());
	}

	private static int getExpectedHashCode(final BirthData birthData) {
		return Objects.hash(birthData.getDate(), birthData.getGender(), birthData.getFirstName(),
				birthData.getMiddleNames(), birthData.getLastName(), birthData.getMother(), birthData.getFather());
	}

	@Test
	void __toString() {
		final BirthData birthData = buildIzukuMidoriyaBirthData();

		final String birthDataStr = birthData.toString();

		assertStringificationContains(birthDataStr, "date", birthData.getDate());
		assertStringificationContains(birthDataStr, "gender", birthData.getGender());
		assertStringificationContains(birthDataStr, "firstName", birthData.getFirstName());
		assertStringificationContains(birthDataStr, "middleNames", birthData.getMiddleNames());
		assertStringificationContains(birthDataStr, "lastName", birthData.getLastName());
		assertStringificationContains(birthDataStr, "mother", birthData.getMother());
		assertStringificationContains(birthDataStr, "father", birthData.getFather());
	}

	private static void assertStringificationContains(final String stringification,
			final String fieldName, final Object fieldValue) {
		final Class<?> fieldValueClass = fieldValue.getClass();

		if (String.class.isAssignableFrom(fieldValueClass)) {
			assertStringificationContainsForStringField(stringification, fieldName, (String) fieldValue);
			return;
		}

		if (Person.class.isAssignableFrom(fieldValueClass)) {
			assertStringificationContainsForStringField(stringification, fieldName, ((Person) fieldValue).getLabel());
			return;
		}

		if (ChronoZonedDateTime.class.isAssignableFrom(fieldValueClass)) {
			assertStringificationContainsForDateTimeField(stringification, fieldName, (TemporalAccessor) fieldValue);
			return;
		}

		if (Genders.class.isAssignableFrom(fieldValueClass)) {
			assertStringificationContainsForGendersField(stringification, fieldName, (Genders) fieldValue);
			return;
		}

		if (Collection.class.isAssignableFrom(fieldValueClass)) {
			assertStringificationContainsForCollectionField(stringification, fieldName, (Collection<?>) fieldValue);
			return;
		}

		throw new IllegalStateException("The \"" + fieldValueClass.getSimpleName() +
				"\" data type is not supported");
	}

	private static void assertStringificationContainsForStringField(final String stringification, final String fieldName,
			final String fieldValue) {
		final String expectedContainedValue = "\"" + fieldName + "\":\"" + fieldValue + "\"";
		assertTrue(stringification.contains(expectedContainedValue));
	}

	private static void assertStringificationContainsForDateTimeField(final String stringification, final String fieldName,
			final TemporalAccessor fieldValue) {
		final String expectedContainedValue = "\"" + fieldName + "\":\"" + DateTimeFormatter.ISO_ZONED_DATE_TIME.format(fieldValue) + "\"";
		assertTrue(stringification.contains(expectedContainedValue));
	}

	private static void assertStringificationContainsForGendersField(final String stringification, final String fieldName,
			final Genders gender) {
		final String expectedContainedValue = "\"" + fieldName + "\":\"" + gender.label() + "\"";
		assertTrue(stringification.contains(expectedContainedValue));
	}

	private static void assertStringificationContainsForCollectionField(final String stringification, final String fieldName,
			final Collection<?> values) {
		if (CollectionUtils.isEmpty(values)) {
			final String expectedContainedValue = "\"" + fieldName + "\":[ ]";
			assertTrue(stringification.contains(expectedContainedValue));
			return;
		}

		final String expectedMatchingRegexp = ".*\"" + fieldName + "\":\\[(\\s?("
				+ values.stream().map(Object::toString).collect(Collectors.joining("|")) + "),?){" + CollectionUtils.size(values) + '}'
				+ "].*";
		assertTrue(stringification.matches(expectedMatchingRegexp));
	}

	@Test
	void constructor_defaultNotAccessible_exceptionThrown() {
		try {
			BirthDataDefaultImpl.class.newInstance();
			fail("An exception should have been thrown");
		} catch (final Exception exception) {
			assertTrue(exception instanceof IllegalAccessException);
		}
	}

	@Test
	void date() {
		final ZonedDateTime date = ZonedDateTime.of(1970, 1, 1, 5, 0, 0, 0, ZoneId.of("UTC"));
		final BirthData birthData = new BirthDataDefaultImpl.Builder().withDate(date).build();
		assertSame(date, birthData.getDate());
	}

	@Test
	void father() {
		final BirthData birthData = buildIzukuMidoriyaBirthData();
		final Person father = buildHisashiMidoriyaSimplifiedRepresentation();
		assertEquals(father, birthData.getFather());
	}

	@Test
	void firstName() {
		final String firstName = "Izuku";
		final BirthData birthData = new BirthDataDefaultImpl.Builder().withFirstName(firstName).build();
		assertSame(firstName, birthData.getFirstName());
	}

	@Test
	void gender() {
		final Genders gender = Genders.MALE;
		final BirthData birthData = new BirthDataDefaultImpl.Builder().withGender(gender).build();
		assertSame(gender, birthData.getGender());
	}

	@Test
	void lastName() {
		final String lastName = "Midoriya";
		final BirthData birthData = new BirthDataDefaultImpl.Builder().withLastName(lastName).build();
		assertSame(lastName, birthData.getLastName());
	}

	@Test
	void middleNames() {
		final String[] middleNames = new String[] { "Ohma", "Raian" };
		final BirthData birthData = new BirthDataDefaultImpl.Builder().withMiddleNames(middleNames).build();

		final Set<String> birthDataMiddleNames = birthData.getMiddleNames();

		for (final String middleName : middleNames) {
			assertTrue(birthDataMiddleNames.contains(middleName), "\"" + middleName + "\" should be present");
		}
	}

	@Test
	void mother() {
		final BirthData birthData = buildIzukuMidoriyaBirthData();
		final Person mother = buildInkoMidoriyaSimplifiedRepresentation();
		assertEquals(mother, birthData.getMother());
	}
}
