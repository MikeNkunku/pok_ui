package com.nkunku.trainings.pokui.domain;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class MaritalStatusesTest {

	@ParameterizedTest
	@MethodSource("fromSource")
	void from(final EnumFromTestDataHolder<MaritalStatuses> dataHolder) {
		assertSame(dataHolder.getExpectedValue(), MaritalStatuses.from(dataHolder.getId()));
	}

	static Iterable<EnumFromTestDataHolder<MaritalStatuses>> fromSource() {
		final EnumFromTestDataHolder<MaritalStatuses> civilPartnerTestData = new EnumFromTestDataHolder<>("\"civilPartner\" for \"CIVIL_PARTNER\"",
				"civilPartner", MaritalStatuses.CIVIL_PARTNER);
		final EnumFromTestDataHolder<MaritalStatuses> divorcedTestData = new EnumFromTestDataHolder<>(
				"\"divorced\" for \"DIVORCED\"", "divorced", MaritalStatuses.DIVORCED);
		final EnumFromTestDataHolder<MaritalStatuses> marriedTestData = new EnumFromTestDataHolder<>(
				"\"married\" for \"MARRIED\"", "married", MaritalStatuses.MARRIED);
		final EnumFromTestDataHolder<MaritalStatuses> singleTestData = new EnumFromTestDataHolder<>(
				"\"single\" for \"SINGLE\"", "single", MaritalStatuses.SINGLE);
		final EnumFromTestDataHolder<MaritalStatuses> widowedTestData = new EnumFromTestDataHolder<>(
				"\"widowed\" for \"WIDOWED\"", "widowed", MaritalStatuses.WIDOWED);

		return Arrays.asList(civilPartnerTestData, divorcedTestData, marriedTestData, singleTestData, widowedTestData);
	}

	@Test
	void from_unknownIdentifier_exceptionThrown() {
		try {
			MaritalStatuses.from("unknownIdentifier");
			fail("An exception should have been thrown");
		} catch (final Exception exception) {
			assertTrue(exception instanceof IllegalArgumentException, "It should be an IllegalArgumentException");
		}
	}

	@ParameterizedTest
	@MethodSource("labelSource")
	void label(final Entry<MaritalStatuses, String> labelEntry) {
		final MaritalStatuses maritalStatus = labelEntry.getKey();
		final String expectedLabel = labelEntry.getValue();
		assertEquals(expectedLabel, maritalStatus.label(),
				String.format("The label for \"%s\" should be \"%s\"", maritalStatus, expectedLabel));
	}

	static Iterable<Entry<MaritalStatuses, String>> labelSource() {
		final EnumMap<MaritalStatuses, String> enumMap = new EnumMap<>(MaritalStatuses.class);
		enumMap.put(MaritalStatuses.CIVIL_PARTNER, "In a civil union");
		enumMap.put(MaritalStatuses.DIVORCED, "Divorced");
		enumMap.put(MaritalStatuses.MARRIED, "Married");
		enumMap.put(MaritalStatuses.SINGLE, "Single");
		enumMap.put(MaritalStatuses.WIDOWED, "Widowed");
		return enumMap.entrySet();
	}
}
