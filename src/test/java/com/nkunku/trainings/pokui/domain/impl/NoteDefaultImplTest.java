package com.nkunku.trainings.pokui.domain.impl;

import java.util.Objects;

import org.junit.jupiter.api.Test;

import com.nkunku.trainings.pokui.domain.Note;

import static org.junit.jupiter.api.Assertions.*;

class NoteDefaultImplTest extends PersistableEntityTestSupport<NoteDefaultImpl> {

	@Test
	void __equal() {
		final Note note = new NoteDefaultImpl("code1", "title1", "content1");

		assertEquals(note, note);

		assertNotEquals(note, new PersonDefaultImpl.Builder().build());
		assertNotEquals(note, note.getCode());
	}

	@Test
	void __hashCode() {
		final int expectedHash = Objects.hash(null, "code1", "title1", "content1");
		assertEquals(expectedHash, new NoteDefaultImpl("code1", "title1", "content1").hashCode(),
				"The hash code values should be equal.");
	}

	@Test
	void __toString() {
		final Note note = new NoteDefaultImpl("code1", "title1", "content1");

		final String noteStr = note.toString();

		assertStringificationContainsFieldWithValue(noteStr, "code", note.getCode());
		assertStringificationContainsFieldWithValue(noteStr, "title", note.getTitle());
		assertStringificationContainsFieldWithValue(noteStr, "content", note.getContent());

		assertStringificationContainsFieldWithValue(noteStr, "id", null);
		assertStringificationContainsFieldWithValue(noteStr, "creationDate", null);
		assertStringificationContainsFieldWithValue(noteStr, "lastModificationDate", null);
		assertStringificationContainsFieldWithValue(noteStr, "softDeletionDate", null);
	}

	@Test
	void constructor_default() {
		final NoteDefaultImpl note = new NoteDefaultImpl();

		assertNull(note.getId(), "The note ID should be null.");
		assertEquals("", note.getCode(), "The note code should be null.");
		assertEquals("", note.getTitle(), "The note title should be null.");
		assertEquals("", note.getContent(), "The note content should be null.");
		assertNull(note.getCreationDate(), "The note creation date should be null.");
		assertNull(note.getLastModificationDate(), "The note last modification date should be null.");
	}

	@Test
	void constructor_parameterized() {
		final String code = "code";
		final String title = "title";
		final String content = "content";

		final Note note = new NoteDefaultImpl(code, title, content);

		assertSame(code, note.getCode(), "The note code should be equal.");
		assertSame(title, note.getTitle(), "The note title should be equal.");
		assertSame(content, note.getContent(), "The note content should be equal.");
	}
}
