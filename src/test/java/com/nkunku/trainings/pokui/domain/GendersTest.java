package com.nkunku.trainings.pokui.domain;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class GendersTest {

	@ParameterizedTest
	@MethodSource(value = "fromSource")
	void from(final EnumFromTestDataHolder<Genders> testDataHolder) {
		assertSame(testDataHolder.getExpectedValue(), Genders.from(testDataHolder.getId()));
	}

	static Iterable<EnumFromTestDataHolder<Genders>> fromSource() {
		return Arrays.asList(new EnumFromTestDataHolder<>("\"F\" for FEMALE", "F", Genders.FEMALE),
				new EnumFromTestDataHolder<>("\"M\" for MALE", "M", Genders.MALE));
	}

	@Test
	void from_unknownIdentifier_exceptionThrown() {
		try {
			Genders.from("unknownIdentifier");
			fail("An exception should have been thrown");
		} catch (final Exception exception) {
			assertTrue(exception instanceof IllegalArgumentException, "It should be an IllegalArgumentException");
		}
	}

	@ParameterizedTest
	@EnumSource(Genders.class)
	void label(final Genders gender) {
		final String genderName = gender.name();
		final String expectedLabel = genderName.charAt(0) + genderName.substring(1).toLowerCase();
		assertEquals(expectedLabel, gender.label());
	}

}
