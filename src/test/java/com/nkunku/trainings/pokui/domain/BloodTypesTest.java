package com.nkunku.trainings.pokui.domain;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class BloodTypesTest {

	private static final String RH_FACTOR_PRESENCE_KEY = "POSITIVE";

	@ParameterizedTest
	@MethodSource("fromSource")
	void from(final EnumFromTestDataHolder<BloodTypes> testDataHolder) {
		assertEquals(testDataHolder.getExpectedValue(), BloodTypes.from(testDataHolder.getId()));
	}

	static Iterable<EnumFromTestDataHolder<BloodTypes>> fromSource() {
		final BloodTypes[] bloodTypes = BloodTypes.values();
		final Collection<EnumFromTestDataHolder<BloodTypes>> fromTestDataHolders = new ArrayList<>(bloodTypes.length);

		addFromDataHolders(bloodTypes, fromTestDataHolders);

		return fromTestDataHolders;
	}

	private static void addFromDataHolders(final BloodTypes[] bloodTypes, final Collection<EnumFromTestDataHolder<BloodTypes>> fromTestDataHolders) {
		for (final BloodTypes bloodType : bloodTypes) {
			final String bloodTypeId = getComputedIdentifier(bloodType);
			final EnumFromTestDataHolder<BloodTypes> bloodTypesEnumFromTestDataHolder = new EnumFromTestDataHolder<>(
					getTestDataHolderLabel(bloodType, bloodTypeId), bloodTypeId, bloodType);
			fromTestDataHolders.add(bloodTypesEnumFromTestDataHolder);
		}
	}

	private static String getComputedIdentifier(final BloodTypes bloodType) {
		final String[] bloodTypeNameParts = bloodType.name().split("_");
		return bloodTypeNameParts[0] + (RH_FACTOR_PRESENCE_KEY.equals(bloodTypeNameParts[1]) ? '+' : '-');
	}

	private static String getTestDataHolderLabel(final BloodTypes bloodType, final String bloodTypeId) {
		return String.format("\"%s\" for \"%s\"", bloodTypeId, bloodType);
	}

	@Test
	void from_unknownIdentifier_exceptionThrown() {
		try {
			BloodTypes.from("bloodTypeIdentifier");
			fail("An exception should have been thrown");
		} catch (final Exception exception) {
			assertTrue(exception instanceof IllegalArgumentException, "It should be an IllegalArgumentException");
		}
	}

	@ParameterizedTest
	@EnumSource(BloodTypes.class)
	void label(final BloodTypes bloodType) {
		assertEquals(getComputedIdentifier(bloodType), bloodType.label());
	}
}
