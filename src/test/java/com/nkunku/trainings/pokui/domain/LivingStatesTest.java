package com.nkunku.trainings.pokui.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import com.nkunku.trainings.pokui.domain.Person.LivingStates;

import static org.junit.jupiter.api.Assertions.*;

class LivingStatesTest {

	@EnumSource(LivingStates.class)
	@ParameterizedTest
	void from_knownIdentifier_success(final LivingStates livingState) {
		assertEquals(livingState, LivingStates.from(livingState.name().toLowerCase()));
	}

	@Test
	void from_unknownIdentifier_failure() {
		final String unknownIdentifier = "zombieland";

		try {
			LivingStates.from(unknownIdentifier);
			fail("An exception should have been thrown");
		} catch (final Exception exception) {
			assertTrue(exception instanceof IllegalArgumentException);
			assertEquals("No living state found for the \"" + unknownIdentifier + "\" identifier", exception.getMessage());
		}
	}

	@EnumSource(LivingStates.class)
	@ParameterizedTest
	void label(final LivingStates livingState) {
		final String name = livingState.name();
		final String expectedLabel = name.charAt(0) + name.substring(1).toLowerCase();
		assertEquals(expectedLabel, livingState.label());
	}
}
