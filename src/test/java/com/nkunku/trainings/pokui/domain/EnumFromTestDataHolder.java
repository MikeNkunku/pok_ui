package com.nkunku.trainings.pokui.domain;

class EnumFromTestDataHolder<ENUM extends Enum<?>> {

	private final String label;
	private final ENUM expectedValue;
	private final String id;

	EnumFromTestDataHolder(final String labelValue, final String identifier, final ENUM enumValue) {
		label = labelValue;
		id = identifier;
		expectedValue = enumValue;
	}

	public ENUM getExpectedValue() {
		return expectedValue;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return label;
	}
}
