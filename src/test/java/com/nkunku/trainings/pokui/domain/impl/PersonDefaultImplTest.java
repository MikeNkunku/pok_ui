package com.nkunku.trainings.pokui.domain.impl;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Objects;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

import com.nkunku.trainings.pokui.domain.BloodTypes;
import com.nkunku.trainings.pokui.domain.Genders;
import com.nkunku.trainings.pokui.domain.MaritalStatuses;
import com.nkunku.trainings.pokui.domain.Person;
import com.nkunku.trainings.pokui.domain.impl.PersonDefaultImpl.Builder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PersonDefaultImplTest extends PersistableEntityTestSupport<PersonDefaultImpl> {

	private static final ChronoZonedDateTime<LocalDate> NARUTO_BIRTH_DATE = ZonedDateTime.of(1999, 10, 10, 3, 26, 59, 0, ZoneId.of("GMT+9"));

	@Test
	void __equals() {
		final Person narutoSimplifiedRepresentation = buildNarutoSimplifiedRepresentation();
		assertEquals(narutoSimplifiedRepresentation, narutoSimplifiedRepresentation);

		assertNotEquals(narutoSimplifiedRepresentation, narutoSimplifiedRepresentation.getAge());
		assertNotEquals(narutoSimplifiedRepresentation, new NoteDefaultImpl());

		final Person hinataSimplifiedRepresentation = buildHinataSimplifiedRepresentation();
		assertNotEquals(narutoSimplifiedRepresentation, hinataSimplifiedRepresentation);
	}

	@Test
	void __hashCode() {
		final Person narutoSimplifiedRepresentation = buildNarutoSimplifiedRepresentation();
		final int expectedHashCode = getExpectedHashCodeFrom(narutoSimplifiedRepresentation);
		assertEquals(expectedHashCode, narutoSimplifiedRepresentation.hashCode());
	}

	private static int getExpectedHashCodeFrom(final Person person) {
		return Objects.hash(person.getId(), person.getCode(),
				person.getCurrentFirstName(), person.getBirthFirstName(), person.getNicknames(),
				person.getCurrentLastName(), person.getBirthLastName(),
				person.getCurrentGender(), person.getBirthGender(),
				person.getBirthDate(), person.getDeathDate(),
				person.getMother(), person.getFather(),
				person.getBloodType(),
				person.getMaritalStatus(), person.getLifePartner(),
				person.getSiblings(), person.getChildren());
	}

	@Test
	void __toString() {
		final Person narutoSimplifiedRepresentation = buildNarutoSimplifiedRepresentation();

		final String narutoStr = narutoSimplifiedRepresentation.toString();

		assertStringificationContainsFieldWithValue(narutoStr, "birthFirstName", "Naruto");
		assertStringificationContainsFieldWithValue(narutoStr, "currentFirstName", "Naruto");
		assertStringificationContainsFieldWithValue(narutoStr, "birthLastName", "Uzumaki");
		assertStringificationContainsFieldWithValue(narutoStr, "currentLastName", "Uzumaki");
		assertStringificationContainsFieldWithValue(narutoStr, "code", "NARUTO_UZUMAKI");
		assertStringificationContainsFieldWithValue(narutoStr, "bloodType", "B+");
		assertStringificationContainsCollectionFieldWithValues(narutoStr, "nicknames",
				Function.identity(), narutoSimplifiedRepresentation.getNicknames());
		assertStringificationContainsFieldWithValue(narutoStr, "mother", "Kushina Uzumaki");
		assertStringificationContainsFieldWithValue(narutoStr, "father", "Minato Namikaze");
		assertStringificationContainsFieldWithValue(narutoStr, "lifePartner", "Hinata Uzumaki");
		assertStringificationContainsFieldWithValue(narutoStr, "maritalStatus", "Married");
		assertStringificationContainsCollectionFieldWithValues(narutoStr, "children",
				Person::getLabel, narutoSimplifiedRepresentation.getChildren());
		assertStringificationContainsFieldWithValue(narutoStr, "siblings", "[  ]");
		assertStringificationContainsFieldWithValue(narutoStr, "birthDate", DateTimeFormatter.ISO_ZONED_DATE_TIME.format(NARUTO_BIRTH_DATE));

		assertStringificationContainsFieldWithValue(narutoStr, "id", null);
		assertStringificationContainsFieldWithValue(narutoStr, "creationDate", null);
		assertStringificationContainsFieldWithValue(narutoStr, "lastModificationDate", null);
		assertStringificationContainsFieldWithValue(narutoStr, "softDeletionDate", null);
	}

	private static Person buildNarutoSimplifiedRepresentation() {
		return new Builder().withBirthFirstName("Naruto").withBirthLastName("Uzumaki")
				.withCode("NARUTO_UZUMAKI")
				.withCurrentFirstName("Naruto").withCurrentLastName("Uzumaki")
				.withBirthGender(Genders.MALE).withCurrentGender(Genders.MALE)
				.withBloodType(BloodTypes.B_POSITIVE)
				.withMother(buildKushinaSimplifiedRepresentation())
				.withFather(buildMinatoSimplifiedRepresentation())
				.withMaritalStatus(MaritalStatuses.MARRIED).withLifePartner(buildHinataSimplifiedRepresentation())
				.withChildren(buildBorutoSimplifiedRepresentation(), buildHimawariSimplifiedRepresentation())
				.withBirthDate(NARUTO_BIRTH_DATE)
				.withSiblings(Collections.emptySet())
				.withNicknames("Child of the Prophecy", "Nanadaime Hokage", "Fox")
				.build();
	}

	private static Person buildKushinaSimplifiedRepresentation() {
		return new Builder().withBirthFirstName("Kushina").withBirthLastName("Uzumaki")
				.withBirthGender(Genders.FEMALE).withCode("KUSHINA_UZUMAKI")
				.withCurrentFirstName("Kushina").withCurrentLastName("Uzumaki")
				.withBloodType(BloodTypes.B_POSITIVE)
				.withNicknames("Tomato", "Hot Habanero")
				.withMaritalStatus(MaritalStatuses.MARRIED)
				.build();
	}

	private static Person buildMinatoSimplifiedRepresentation() {
		return new Builder().withBirthFirstName("Minato").withBirthLastName("Namikaze")
				.withCode("MINATO_NAMIKAZE")
				.withCurrentFirstName("Minato").withCurrentLastName("Namikaze")
				.withBirthGender(Genders.MALE)
				.withBloodType(BloodTypes.B_POSITIVE)
				.withNicknames("Yondaime Hokage", "Yellow Flash of the Leaf")
				.withMaritalStatus(MaritalStatuses.MARRIED)
				.build();
	}

	private static Person buildHinataSimplifiedRepresentation() {
		return new Builder().withBirthFirstName("Hinata").withBirthLastName("Hyuga")
				.withBirthGender(Genders.FEMALE).withCode("HINATA_HYUGA")
				.withCurrentFirstName("Hinata").withCurrentLastName("Uzumaki")
				.withBloodType(BloodTypes.A_NEGATIVE)
				.withNicknames("Byakugan Princess")
				.withMaritalStatus(MaritalStatuses.MARRIED)
				.build();
	}

	private static Person buildBorutoSimplifiedRepresentation() {
		return new Builder().withBirthFirstName("Boruto").withBirthLastName("Uzumaki")
				.withCode("BORUTO_UZUMAKI").withBirthGender(Genders.MALE)
				.withCurrentFirstName("Boruto").withCurrentLastName("Uzumaki")
				.withBloodType(BloodTypes.AB_POSITIVE).build();
	}

	private static Person buildHimawariSimplifiedRepresentation() {
		return new Builder().withBirthFirstName("Himawari").withBirthLastName("Uzumaki")
				.withCode("HIMAWARI_UZUMAKI").withBirthGender(Genders.FEMALE)
				.withCurrentFirstName("Himawari").withCurrentLastName("Uzumaki")
				.withBloodType(BloodTypes.AB_NEGATIVE).build();
	}
}
