package com.nkunku.trainings.pokui.domain.impl;

import java.time.chrono.ChronoZonedDateTime;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class PersistableEntityTestSupport<PERSISTABLE_ENTITY extends PersistableEntity<?, ?>> {

	protected <COMPONENT_TYPE> void assertStringificationContainsCollectionFieldWithValues(final String stringification,
			final String collectionFieldName, final Function<COMPONENT_TYPE, String> stringValueExtractor,
			final Collection<? extends COMPONENT_TYPE> values) {
		final String regexpToMatch = getExpectedRegExpForCollectionField(collectionFieldName, stringValueExtractor, values);
		assertTrue(stringification.matches(regexpToMatch));
	}

	private static <COMPONENT_TYPE> String getExpectedRegExpForCollectionField(final String collectionFieldName,
			final Function<COMPONENT_TYPE, String> stringValueExtractor, final Collection<? extends COMPONENT_TYPE> values) {
		final String valuesRegExp = "(" + values.stream().map(stringValueExtractor).collect(Collectors.joining("|")) + ")";

		return ".*\"" + collectionFieldName + "\":\"\\[\\s?(,?\\s?" + valuesRegExp + "){" + values.size() + "}\\s?\\]\".*";
	}

	protected <VALUE_TYPE> void assertStringificationContainsFieldWithValue(final String stringification, final String field,
			final VALUE_TYPE value) {
		final String expectedFieldStr = "\"" + field + "\":" + getStringifiedValue(value);
		assertTrue(stringification.contains(expectedFieldStr),
				"\""+ value+ "\" should be the value for \"" + field + "\" but it wasn't found in " + stringification);
	}

	private static <VALUE_TYPE> String getStringifiedValue(final VALUE_TYPE value) {
		if (value == null) {
			return null;
		}

		final Class<?> valueClass = value.getClass();
		if (valueClass.isAssignableFrom(Number.class)) {
			return String.valueOf(value);
		}

		if (valueClass.isAssignableFrom(ChronoZonedDateTime.class)) {
			return value.toString();
		}

		if (valueClass.isAssignableFrom(Collection.class)) {
			return '[' + ((Collection<?>) value).stream().map(Objects::toString).collect(Collectors.joining(", ")) + ']';
		}

		return "\"" + value + "\"";
	}
}
